#!/bin/sh
docker build -t sites:latest .
docker-compose -f docker-compose-test.yml up -d
sleep 20
docker container ps
docker-compose -f docker-compose-test.yml exec -T web python ./manage.py test --no-input