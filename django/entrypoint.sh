#!/bin/bash
sleep 20
python manage.py migrate
python manage.py collectstatic --no-input
gunicorn --bind 0.0.0.0:$PORT core.wsgi