FROM python:latest
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
COPY www/public /code
COPY django/requirements.txt /code/
COPY django/entrypoint.sh /code/
WORKDIR /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 8000
RUN chmod u+x entrypoint.sh
CMD ["./entrypoint.sh"]